@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')

<!--  Este del formulario inicio de  gestion academida -->
<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Consulta de usuarios</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">					
<!-- Contenido de Captura de expedientes-->
<!-- Cuadros de menu -->  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title"></div>
                    </div>
                <div class="card-body">
                <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
  </tbody>
</table>
                                   
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Fin de formulario de gestion academida -->



@endsection
