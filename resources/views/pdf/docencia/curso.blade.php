@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')

	<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Docencia</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Cursos</div>
                    </div>
                	<div class="card-body">
                		<form action="{{ url('pdf_download_Curso') }}" method="post" accept-charset="utf-8">
                        @csrf
	                        <div class="form-row">
	                            <div class="form-group col-md-4">
	                                <label for="inputEmail4">Nombre del curso</label>
	                                <input type="text" class="form-control border border-secondary" id="inputEmail4" placeholder="Nombre del curso" name="nombre">
	                            </div>
	                            <div class="form-group col-md-4">
	                                <label for="inputPassword4">Institucion de Educacion Superior (IES)</label>
	                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Institucion de Educacion Superior (IES)" name="institucion">
	                            </div>
	                            <div class="form-group col-md-4">
	                                <label for="inputPassword4">Programa educativo</label>
	                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Programa educativo" name="programa">
	                            </div>
	                            <div class="form-group col-md-4">
	                                <label for="inputPassword4">Cuatrimestre</label>
	                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Cuatrimeste" name="cuatri">
	                            </div>
	                            <div class="form-group col-md-4">
	                                <label for="inputPassword4">Grupo</label>
	                                <input type="text" class="form-control border border-secondary" id="inputPassword4" placeholder="Grupo" name="grupo">
	                            </div>  
	                            <div class="form-group col-md-4">
	                                <label for="inputPassword4">Nivel</label>
	                                <select class="custom-select border border-secondary" id="inputGroupSelect01"  name="nivel">
	                                    <option value="Tecnico Superior Universitario">Tecnico Superior Universitario</option>
	                                    <option value="Ingenieria">Ingenieria</option>      
                                	</select>
	                            </div> 
	                            <div class="form-group col-md-3">
	                                <label for="inputCity">Fecha de inicio</label>
	                                <input type="date" class="form-control border border-secondary" id="inputDate" name="fecha">
	                            </div>
	                            <div class="form-group col-md-3">
	                                <label for="inputPassword4">Numero de alumnos</label>
	                                <input type="number" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre" name="alumnos">
	                            </div> 
	                            <div class="form-group col-md-3">
	                                <label for="inputPassword4">Duracion en semanas</label>
	                                <input type="number" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre" name="duracion">
	                            </div> 
	                            <div class="form-group col-md-3">
	                                <label for="inputPassword4">Horas de asesoria al mes</label>
	                                <input type="number" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre" name="hMes">
	                            </div> 
	                            <div class="form-group col-md-3">
	                                <label for="inputPassword4">Horas semanales dedicadas a este curso</label>
	                                <input type="number" class="form-control border border-secondary" id="inputPassword4" placeholder="Escribir Nombre" name="hSemana">
	                            </div>
	                        </div>
	                        <div class="form-row">
	                            <br>
	                            <button type="submit" class="btn btn-primary">Generar Archivo</button>
	                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection