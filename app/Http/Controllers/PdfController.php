<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Pdef;

class PdfController extends Controller
{
  //Metodos PDF 
  public function pdfForm()
  {
    return view('expedientes.create');
  }

  public function pdfDownload(Request $request){
    request()->validate([
      'Titulo' => 'required',
      'Asunto' => 'required',
      'Date' => 'required',
      'Descripcion' => 'required',
      'Docente' => 'required',
      'Area' => 'required'
    ]);

    Pdef::create([
      'titulo' => $request->Titulo,
      'asunto' => $request->Asunto,
      'date' => $request->Date,
      'descripcion' => $request->Descripcion,
      'docente' => $request->Docente,
      'area' => $request->Area,
      'tip' => "material",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'titulo' => $request->Titulo,
      'asunto' => $request->Asunto,
      'date' => $request->Date,
      'descripcion' => $request->Descripcion,
      'docente' => $request->Docente,
      'area' => $request->Area
    ];

    $pdf = PDF::loadView('pdf_download', $data);

    return $pdf->stream('informe.pdf');
  }
  //PDF Articulo
  public function pdfFormArticulo()
  {
    return view('pdf.academica.Articulo');
  }

  public function pdfDownload_Articulo(Request $request){
    request()->validate([
      'autor' => 'required',
      'titulo' => 'required',
      'estado' => 'required',
      'pais' => 'required',
      'nombre' => 'required',
      'editorial' => 'required',
      'volumen' => 'required',
      'issn' => 'required',
      'proposito' => 'required',
      'date' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
      'cv' => 'required'
    ]);

    Pdef::create([
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'nombre' => $request->nombre,
      'editorial' => $request->editorial,
      'volumen' => $request->volumen,
      'issn' => $request->issn,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'tip' => "articulo",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'nombre' => $request->nombre,
      'editorial' => $request->editorial,
      'volumen' => $request->volumen,
      'issn' => $request->issn,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Articulo', $data);

    return $pdf->stream('informeGestion.pdf');
  }


  //PDF Material
  public function pdfFormMaterial()
  {
    return view('pdf.academica.Material');
  }

  public function pdfDownload_Material(Request $request){
    request()->validate([
      'autor' => 'required',
      'titulo' => 'required',
      'descripcion' => 'required',
      'pais' => 'required',
      'editorial' => 'required',
      'proposito' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
      'cv' => 'required',
      'date' => 'required',
      'area' => 'required'
    ]);

    Pdef::create([
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'descripcion' => $request->descripcion,
      'pais' => $request->pais,
      'editorial' => $request->editorial,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'area' => $request->area,
      'tip' => "material",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'descripcion' => $request->descripcion,
      'pais' => $request->pais,
      'editorial' => $request->editorial,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'area' => $request->area
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Material', $data);

    return $pdf->stream('informeGestion.pdf');
  }

  //PDF Asesorias
  public function pdfFormAsesorias()
  {
    return view('pdf.academica.Asesorias');
  }

  public function pdfDownload_Asesorias(Request $request){
    request()->validate([
      'nombre' => 'required',
      'alcance' => 'required',
      'empresa' => 'required',
      'pais' => 'required',
      'estado' => 'required',
      'investigadores' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
      'cv' => 'required',
      'date' => 'required'
    ]);

    Pdef::create([
      'nombre' => $request->nombre,
      'alcance' => $request->alcance,
      'empresa' => $request->empresa,
      'pais' => $request->pais,
      'estado' => $request->estado,
      'investigadores' => $request->investigadores,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'tip' => "asesoria",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'nombre' => $request->nombre,
      'alcance' => $request->alcance,
      'empresa' => $request->empresa,
      'pais' => $request->pais,
      'estado' => $request->estado,
      'investigadores' => $request->investigadores,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Asesorias', $data);

    return $pdf->stream('informeGestion.pdf');
  }
  //PDF Proyectos
  public function pdfFormProyecto()
  {
    return view('pdf.academica.proyecto');
  }

  public function pdfDownload_Proyectos(Request $request){
    request()->validate([
      'titulo' => 'required',
      'patrocinador' => 'required',
      'tipo' => 'required',
      'alumnos' => 'required',
      'actividades' => 'required',
      'investigadores' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
      'cv' => 'required',
      'termino' => 'required',
      'inicio' => 'required'
    ]);

    Pdef::create([
      'titulo' => $request->titulo,
      'patrocinador' => $request->patrocinador,
      'tipo' => $request->tipo,
      'alumnos' => $request->alumnos,
      'actividades' => $request->actividades,
      'investigadores' => $request->investigadores,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'inicio' => $request->inicio,
      'termino' => $request->termino,
      'tip' => "proyectos",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'titulo' => $request->titulo,
      'patrocinador' => $request->patrocinador,
      'tipo' => $request->tipo,
      'alumnos' => $request->alumnos,
      'actividades' => $request->actividades,
      'investigadores' => $request->investigadores,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'inicio' => $request->inicio,
      'termino' => $request->termino
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Proyecto', $data);

    return $pdf->stream('informeGestion.pdf');
  }
  //PDF Revistas
  public function pdfFormRevista()
  {
    return view('pdf.academica.Articulo_revista');
  }

  public function pdfDownload_Revista(Request $request){
    request()->validate([
      'autor' => 'required',
      'titulo' => 'required',
      'descripcion' => 'required',
      'estado' => 'required',
      'pais' => 'required',
      'nombre' => 'required',
      'editorial' => 'required',
      'pagina' => 'required',
      'pagina2' => 'required',
      'volumen' => 'required',
      'indice' => 'required',
      'issn' => 'required',
      'direccion' => 'required',
      'proposito' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
      'cv' => 'required',
      'date' => 'required',
      'area' => 'required'
    ]);

    Pdef::create([
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'descripcion' => $request->descripcion,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'nombre' => $request->nombre,
      'editorial' => $request->editorial,
      'pagina' => $request->pagina,
      'pagina2' => $request->pagina2,
      'volumen' => $request->volumen,
      'indice' => $request->indice,
      'issn' => $request->issn,
      'direccion' => $request->direccion,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'area' => $request->area,
      'tip' => "revista",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'descripcion' => $request->descripcion,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'nombre' => $request->nombre,
      'editorial' => $request->editorial,
      'pagina' => $request->pagina,
      'pagina2' => $request->pagina2,
      'volumen' => $request->volumen,
      'indice' => $request->indice,
      'issn' => $request->issn,
      'direccion' => $request->direccion,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'area' => $request->area
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Revista', $data);

    return $pdf->stream('informeGestion.pdf');
  }
  //PDF Arbitrado
  public function pdfFormArbitrado()
  {
    return view('pdf.academica.Articulo_Arbitrado');
  }

  public function pdfDownload_Arbitrado(Request $request){
    request()->validate([
      'autor' => 'required',
      'titulo' => 'required',
      'estado' => 'required',
      'pais' => 'required',
      'nombre' => 'required',
      'editorial' => 'required',
      'volumen' => 'required',
      'issn' => 'required',
      'proposito' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
      'cv' => 'required',
      'date' => 'required',
      'area' => 'required'
    ]);

    Pdef::create([
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'nombre' => $request->nombre,
      'editorial' => $request->editorial,
      'volumen' => $request->volumen,
      'issn' => $request->issn,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'area' => $request->area,
      'tip' => "arbitrado",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'nombre' => $request->nombre,
      'editorial' => $request->editorial,
      'volumen' => $request->volumen,
      'issn' => $request->issn,
      'proposito' => $request->proposito,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'cv' => $request->cv,
      'date' => $request->date,
      'area' => $request->area
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Arbitrado', $data);

    return $pdf->stream('informeGestion.pdf');
  }
  //PDF Libro
  public function pdfFormLibro()
  {
    return view('pdf.academica.libro');
  }

  public function pdfDownload_Libro(Request $request){
    request()->validate([
      'autor' => 'required',
      'titulo' => 'required',
      'estado' => 'required',
      'pais' => 'required',
      'editorial' => 'required',
      'edicion' => 'required',
      'tiraje' => 'required',
      'isbn' => 'required',
      'proposito' => 'required',
      'date' => 'required',
      'area' => 'required'
    ]);

    Pdef::create([
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'editorial' => $request->editorial,
      'edicion' => $request->edicion,
      'tiraje' => $request->tiraje,
      'isbn' => $request->isbn,
      'proposito' => $request->proposito,
      'date' => $request->date,
      'area' => $request->area,
      'tip' => "libro",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'autor' => $request->autor,
      'titulo' => $request->titulo,
      'estado' => $request->estado,
      'pais' => $request->pais,
      'editorial' => $request->editorial,
      'edicion' => $request->edicion,
      'tiraje' => $request->tiraje,
      'isbn' => $request->isbn,
      'proposito' => $request->proposito,
      'date' => $request->date,
      'area' => $request->area
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Libro', $data);

    return $pdf->stream('informeGestion.pdf');
  }
  //PDF Informe Tecnico
  public function pdfFormInforme()
  {
    return view('pdf.academica.informe_tec');
  }

  public function pdfDownload_Informe(Request $request){
    request()->validate([
      'autor' => 'required',
      'nombre' => 'required',
      'alcance' => 'required',
      'empresa' => 'required',
      'año' => 'required',
      'estado' => 'required',
      'paginas' => 'required',
      'pais' => 'required',
      'miembros' => 'required',
      'proposito' => 'required',
      'lgacs' => 'required',
      'cv' => 'required',
      'date' => 'required'
    ]);

    Pdef::create([
      'autor' => $request->autor,
      'nombre' => $request->nombre,
      'alcance' => $request->alcance,
      'empresa' => $request->empresa,
      'año' => $request->año,
      'estado' => $request->estado,
      'paginas' => $request->paginas,
      'pais' => $request->pais,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'proposito' => $request->proposito,
      'cv' => $request->cv,
      'date' => $request->date,
      'tip' => "informe",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'autor' => $request->autor,
      'nombre' => $request->nombre,
      'alcance' => $request->alcance,
      'empresa' => $request->empresa,
      'año' => $request->año,
      'estado' => $request->estado,
      'paginas' => $request->paginas,
      'pais' => $request->pais,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'proposito' => $request->proposito,
      'cv' => $request->cv,
      'date' => $request->date
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Informe', $data);

    return $pdf->stream('informeGestion.pdf');
  }

  public function pdfDownload_Curso(Request $request){
    request()->validate([
      'nombre' => 'required',
      'institucion' => 'required',
      'programa' => 'required',
      'cuatri' => 'required',
      'grupo' => 'required',
      'nivel' => 'required',
      'fecha' => 'required',
      'alumnos' => 'required',
      'duracion' => 'required',
      'hMes' => 'required',
      'hSemana' => 'required',
    ]);

    Pdef::create([
      'nombre' => $request->nombre,
      'institucion' => $request->institucion,
      'programa' => $request->programa,
      'cuatri' => $request->cuatri,
      'grupo' => $request->grupo,
      'nivel' => $request->nivel,
      'fecha' => $request->fecha,
      'alumnos' => $request->alumnos,
      'duracion' => $request->duracion,
      'hMes' => $request->hMes,
      'hSemana' => $request->hSemana,
      'tip' => "curso",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'nombre' => $request->nombre,
      'institucion' => $request->institucion,
      'programa' => $request->programa,
      'cuatri' => $request->cuatri,
      'grupo' => $request->grupo,
      'nivel' => $request->nivel,
      'fecha' => $request->fecha,
      'alumnos' => $request->alumnos,
      'duracion' => $request->duracion,
      'hMes' => $request->hMes,
      'hSemana' => $request->hSemana,
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Curso', $data);

    return $pdf->stream('informeGestion.pdf');
  }

  public function pdfDownload_Tutoria(Request $request){
    request()->validate([
      'nivel' => 'required',
      'programa' => 'required',
      'fechaIni' => 'required',
      'fechaFin' => 'required',
      'tipo' => 'required',
      'estado' => 'required',
    ]);

    Pdef::create([
      'nivel' => $request->nivel,
      'programa' => $request->programa,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'tipo' => $request->tipo,
      'estado' => $request->estado,
      'tip' => "tutoria",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'nivel' => $request->nivel,
      'programa' => $request->programa,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'tipo' => $request->tipo,
      'estado' => $request->estado,
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Tutoria', $data);

    return $pdf->stream('informeGestion.pdf');
  }

  public function pdfDownload_Tutoria_Grupal(Request $request){
    request()->validate([
      'numero' => 'required',
      'nivel' => 'required',
      'programa' => 'required',
      'fechaIni' => 'required',
      'fechaFin' => 'required',
      'tipo' => 'required',
      'estado' => 'required',
    ]);

    // if($request->file('evidencia'))
    // {
    //   $file=$request->file('file');
    //   $nom = $file->getClientOriginalName();
    // }

    Pdef::create([
      'numero' => $request->numero,
      'nivel' => $request->nivel,
      'programa' => $request->programa,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'tipo' => $request->tipo,
      'estado' => $request->estado,
      'evidencia' => $request->evidencia,
      'tip' => "tutoria grupal",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'numero' => $request->numero,
      'nivel' => $request->nivel,
      'programa' => $request->programa,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'tipo' => $request->tipo,
      'estado' => $request->estado,
      'evidencia' => $request->evidencia,
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Tutoria_Grupal', $data);

    return $pdf->stream('informeGestion.pdf');
  }

  public function pdfDownload_Direccion_Individualizada(Request $request){
    request()->validate([
      'titulo' => 'required',
      'nivel' => 'required',
      'fechaIni' => 'required',
      'fechaFin' => 'required',
      'alumnos' => 'required',
      'estado' => 'required',
      'considerar' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
    ]);

    Pdef::create([
      'titulo' => $request->titulo,
      'nivel' => $request->nivel,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'alumnos' => $request->alumnos,
      'estado' => $request->estado,
      'considerar' => $request->considerar,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'tip' => "direccion",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'titulo' => $request->titulo,
      'nivel' => $request->nivel,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'alumnos' => $request->alumnos,
      'estado' => $request->estado,
      'considerar' => $request->considerar,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Direccion_Individualizada', $data);

    return $pdf->stream('informeGestion.pdf');
  }

  public function pdfDownload_Estadia(Request $request){
    request()->validate([
      'nombre' => 'required',
      'programa' => 'required',
      'grado' => 'required',
      'empresa' => 'required',
      'puntos' => 'required',
      'resultados' => 'required',
      'fechaIni' => 'required',
      'fechaFin' => 'required',
      'alumnos' => 'required',
      'horas' => 'required',
      'estado' => 'required',
      'considerar' => 'required',
      'miembros' => 'required',
      'lgacs' => 'required',
    ]);

    Pdef::create([
      'nombre' => $request->nombre,
      'programa' => $request->programa,
      'grado' => $request->grado,
      'empresa' => $request->empresa,
      'puntos' => $request->puntos,
      'resultados' => $request->resultados,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'alumnos' => $request->alumnos,
      'horas' => $request->horas,
      'estado' => $request->estado,
      'considerar' => $request->considerar,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
      'tip' => "estadia",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'nombre' => $request->nombre,
      'programa' => $request->programa,
      'grado' => $request->grado,
      'empresa' => $request->empresa,
      'puntos' => $request->puntos,
      'resultados' => $request->resultados,
      'fechaIni' => $request->fechaIni,
      'fechaFin' => $request->fechaFin,
      'alumnos' => $request->alumnos,
      'horas' => $request->horas,
      'estado' => $request->estado,
      'considerar' => $request->considerar,
      'miembros' => $request->miembros,
      'lgacs' => $request->lgacs,
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Estadia', $data);

    return $pdf->stream('informeGestion.pdf');
  }

  public function pdfDownload_CuerpoAcademico(Request $request){
    request()->validate([
      'nombre' => 'required',
      'clave' => 'required',
      'grado' => 'required',
      'lineas' => 'required',
    ]);

    Pdef::create([
      'nombre' => $request->nombre,
      'clave' => $request->clave,
      'grado' => $request->grado,
      'lineas' => $request->lineas,
      'tip' => "cuerpo academico",
      'user_id' => auth()->id(),
    ]);

    $data = [
      'nombre' => $request->nombre,
      'clave' => $request->clave,
      'grado' => $request->grado,
      'lineas' => $request->lineas,
    ];

    $pdf = PDF::loadView('pdf.download.pdf_download_Cuerpo_Academico', $data);

    return $pdf->stream('informeGestion.pdf');
  }
}